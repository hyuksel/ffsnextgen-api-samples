﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FFS_API_Example.FFSObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FFS_API_Example
{
    /// <summary>
    /// Author Halil Yuksel | support@fulfilment-software.com
    /// License http://creativecommons.org/licenses/MIT/ MIT
    /// </summary>
    class Program
    {
        public const string APIVersion = "v1";
        public const string APPID = "4d53bce03ec34c0a911182d4c228ee6c";
        public const string APPKEY = "A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=";

        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Calling the back-end API");

            string apiBaseAddress = "http://localhost/FFSApi/";

            //apiBaseAddress = "https://api.fulfilment-software.eu/";


            CustomDelegatingHandler customDelegatingHandler = new CustomDelegatingHandler();

            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);

            //** Create new order object and post this object to the FFS API to create a new order.
            var order = new Order { 
                OrderId = "MyTestOrder001",
                OrderDate = DateTime.Now,
                ShippingFirstName = "Halil",
                ShippingLastName = "Yuksel",
                ShippingAddress = "Edisonweg 30",
                ShippingHouseNr = "--",
                ShippingZipcode = "4207 HG",
                ShippingCity = "Gorinchem",
                ShippingCountry = "NLD",
                ShippingMethod =  "POSTNL_@92991_@838882",
                CarrierId = 2                                   //** optional; Set this shippingmethod to be BriefPost in FFS (can also be changed through WebPortal / Admin Settings)
            };

            //** Add some orderlines to this order
            var lstOrderLines = new List<OrderLine>();

            var objLine1 = new OrderLine
            {
                Sku = "IPH-01", 
                Title = "iPhone Case Black", 
                Quantity = 2,
                Tags = "Apple"
            };

            lstOrderLines.Add(objLine1);

            var objLine2 = new OrderLine
            {
                Sku = "IPH-01",
                Title = "iPhone Case Black",
                Quantity = 3
            };

            lstOrderLines.Add(objLine2);

        //    order.OrderLines = lstOrderLines;       //** Add orderlines collection to order object and pass on to API...

            var response = await client.PostAsJsonAsync(apiBaseAddress + string.Format("{0}/orders", APIVersion), order);

            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
                Console.WriteLine("HTTP Status: {0}, Reason {1}. Press ENTER to exit", 
                    response.StatusCode, response.ReasonPhrase);

                if(response.StatusCode== HttpStatusCode.OK)
                {
                    var resp = JsonConvert.DeserializeObject<OrderReponse>(responseString);

                }

            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }

            Console.ReadLine();
        }

        public class CustomDelegatingHandler : DelegatingHandler
        {
            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {

                HttpResponseMessage response = null;
                string requestContentBase64String = string.Empty;

                string requestUri = System.Web.HttpUtility.UrlEncode(request.RequestUri.AbsoluteUri.ToLower());

                string requestHttpMethod = request.Method.Method;

                //Calculate UNIX time
                DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
                TimeSpan timeSpan = DateTime.UtcNow - epochStart;
                string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

                //create random nonce for each request
                string nonce = Guid.NewGuid().ToString("N");

                //Checking if the request contains body, usually will be null wiht HTTP GET and DELETE
                if (request.Content != null)
                {
                    byte[] content = await request.Content.ReadAsByteArrayAsync();
                    MD5 md5 = MD5.Create();
                    //Hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                    byte[] requestContentHash = md5.ComputeHash(content);
                    requestContentBase64String = Convert.ToBase64String(requestContentHash);
                }

                //Creating the raw signature string
                string signatureRawData = String.Format("{0}{1}{2}{3}{4}{5}", APPID, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

                var secretKeyByteArray = Convert.FromBase64String(APPKEY);

                byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

                using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
                {
                    byte[] signatureBytes = hmac.ComputeHash(signature);
                    string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);
                    //Setting the values in the Authorization header using custom scheme (amx)
                    request.Headers.Authorization = new AuthenticationHeaderValue("ffs", string.Format("{0}:{1}:{2}:{3}",APPID, requestSignatureBase64String, nonce, requestTimeStamp));
                }

                response = await base.SendAsync(request, cancellationToken);

                return response;
            }
        }

        private void GenerateAPPKey()
        {
            using (var cryptoProvider = new RNGCryptoServiceProvider())
            {
                byte[] secretKeyByteArray = new byte[32]; //256 bit
                cryptoProvider.GetBytes(secretKeyByteArray);
                var APIKey = Convert.ToBase64String(secretKeyByteArray);
            }
        }
    }
}
