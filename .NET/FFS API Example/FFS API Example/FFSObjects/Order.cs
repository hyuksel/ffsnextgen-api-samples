﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFS_API_Example.FFSObjects
{
    public class Order
    {

        public string OrderId { get; set; }                             // Required Field

        public DateTime OrderDate { get; set; }

        public int OrderStatusId { get; set; }
  
        public string ShippingMethod { get; set; }

        public string ShippingMethodOptions { get; set; }

        public byte CarrierId { get; set; }                             //** Optional; Get Id's from API/CARRIERS

        public string Tags { get; set; }    

        public string CurrencyCode { get; set; }

        public string ContactFirstName { get; set; }

        public string ContactLastname { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhoneNumber { get; set; }

        public string ContactMobileNumber { get; set; }

        public bool ContactGender { get; set; }

        public DateTime ContactBirthDay { get; set; }

        public string BillingFirstName { get; set; }

        public string BillingLastName { get; set; }                     // Required Field (ONLY IF BILLING ADDRESS IS USED, OTHERWISE OPTIONAL)

        public string BillingCompanyName { get; set; }

        public string BillingAddress { get; set; }                      // Required Field (ONLY IF BILLING ADDRESS IS USED, OTHERWISE OPTIONAL)

        public string BillingAddress2 { get; set; }

        public string BillingHouseNr { get; set; }                      // Required Field (ONLY IF BILLING ADDRESS IS USED, OTHERWISE OPTIONAL)

        public string BillingHouseNrExt { get; set; }

        public string BillingZipcode { get; set; }                      // Required Field (ONLY IF BILLING ADDRESS IS USED, OTHERWISE OPTIONAL)

        public string BillingProvince { get; set; }

        public string BillingCity { get; set; }                         // Required Field (ONLY IF BILLING ADDRESS IS USED, OTHERWISE OPTIONAL)

        public string BillingCountry { get; set; }                      // Required Field (ISO 2/ ISO3 CountryCode)

        public string ShippingFirstName { get; set; }

        public string ShippingLastName { get; set; }                    //Required

        public string ShippingCompanyName { get; set; }

        public string ShippingAddress { get; set; }                     //Required

        public string ShippingAddress2 { get; set; }

        public string ShippingHouseNr { get; set; }                     //Required

        public string ShippingHouseNrExt { get; set; }

        public string ShippingZipcode { get; set; }                     //Required

        public string ShippingProvince { get; set; }

        public string ShippingCity { get; set; }                        //Required

        public string ShippingCountry { get; set; }                     //Required

        public string Comments { get; set; }

        public Decimal PriceCost { get; set; }

        public Decimal PriceExcl { get; set; }

        public Decimal PriceIncl { get; set; }

        public Decimal PriceTaxRate { get; set; }
        public List<OrderLine> OrderLines { get; set; } 

    }

    public class OrderLine
    {
        public string Title { get; set; }               // Required

        public string Sku { get; set; }                   // Required

        public string Ean { get; set; }                 // Max length 13

        public string Tags { get; set; }                //** You can add multiple tags by seperating words with SPACE ; eg  iphones Apple

        public Decimal Quantity { get; set; }
        public bool HasGiftCard { get; set; }
     
        public string GiftCardText { get; set; }

        public bool GiftWrapped { get; set; }
    }

    public class OrderReponse
    {
        public int Id { get; set; }
        public int ChannelId { get; set; }
    }
}
