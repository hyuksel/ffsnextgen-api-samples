<?php

namespace FulfilmentSoftware\API;

/**
 * Fulfilment-Software - PHP API Client
 *
 * @author Halil Yuksel | support@fulfilment-software.com
 * @license http://creativecommons.org/licenses/MIT/ MIT
 */

class FFSClient {

	protected $apiUrl = 'http://localhost/FFSApi/';
    //protected $apiUrl = 'https://api.fulfilment-software.eu/';
    protected $libVersion = 'v1.0';
	protected $apiVersion = 'v1';
    protected $isDebug = true;
    protected $appId = '';
	protected $apiKey = '';

    public function __construct($appId, $apiKey)
    {
        $this->appId = $appId;
        $this->apiKey = $apiKey;
    }

    public function getOrders()
    {
         $QueryFilterParams = array(
            'ids'          => 'DKS-092383'
        );

        $this->processRequest("GET", "orders", $QueryFilterParams);
    }

    public function getOrder($id)
    {


    }
    
    public function addOrder($id)
    {

        $QueryFil = array(
            'productcode'          => 'DKS-092383',
            'productcode_supplier' => 'DKS-092383',
            'name'                 => 'Apple iPod Shuffle Purple',
            'price'                => 59.95,
            'fixedstockprice'      => 49.95,
            'weight'               => 500,
            'barcode'              => '9983762736271',
            'idvatgroup'           => $vatgroups['data'][0]['idvatgroup'] // First VAT group in Picqer
        );

    }

    //** Each request must be signed with a valid authorization header
    protected function processRequest($method = 'GET', $uri, $params, $postData = null)
    {
        $urlParams = '';

        $requestUrl = $this->apiUrl.$this->apiVersion."/".$uri;

        if($params != null){
            
            $urlParams  = http_build_query($params);

            $requestUrl = $requestUrl.'?'.$urlParams;
        }

        if($this->isDebug){
             echo "RequestUrl: ".$requestUrl."</br></br>";  
        }

        $time = time();     //** Unix Time
        $nonce = $this->makeRandomString(10);

        if($this->isDebug){
             echo "nonce: ".$nonce."</br></br></br>";  
        }

        $content = '';

        if($content != ''){
            $content = base64_encode(md5($content));
        }

        $signatureRawData = $this->appId.$method.strtolower(urlencode(strtolower($requestUrl))).$time.$nonce.$content;
    
        if($this->isDebug){
             echo "signatureRawData: ".$signatureRawData."</br></br></br>";  
        }

        $requestSignature = base64_encode(hash_hmac('sha256', $signatureRawData, base64_decode($this->apiKey), true));

        $authHeader = 'Authorization: ffs '.$this->appId.":".$requestSignature.":".$nonce.":".$time;

        if($this->isDebug){
             echo "Authorization Header: ".$authHeader."</br></br></br>";  
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_HEADER, FAlSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Fulfilment-Software PHP API Client | ' . $this->libVersion);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

        $data = '';

        if ($method == 'POST' || $method == 'PUT' || $method == 'DELETE')
        {
            $data = $this->convertToJsonData($params);

            if ($this->isDebug)
            {
                echo 'Data: ' . $data . PHP_EOL;
            }

            if ($method == 'POST')
            {
                curl_setopt($ch, CURLOPT_POST, true);

            } elseif ($method == 'PUT')
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

            } elseif ($method == 'DELETE')
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }


        //** Set Request Authorization Header ; Without this our API can not validate your request
        curl_setopt($ch, CURLOPT_HTTPHEADER, 
            array('Content-Type: application/json', 'Accept: application/json', $authHeader, 'Content-Length: ' . strlen($data)));


        $result = curl_exec($ch);

        if ($this->isDebug)
        {
            echo $result . PHP_EOL;
        }

        curl_close($ch);

    }

    protected function makeRandomString($length = 10)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    protected function convertToJsonData($params)
    {
        $data = json_encode($params);
        return $data;
    }

}//--

?>